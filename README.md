#citation 1:
Votre temps est limité, ne le gacher pas en menant une existence qui n'est pas la votre. Sortez des dogmes qui vous gardent prisonnier et qui vous obligent à obeir à la pensée d'autrui. Ne laissez pas le brouhaha exterieur étouffer votre voix interieur. Osez de suivre votre coeur et votre intuition. L'un et l'autre savent ce que vous voulez reéllement devenir. Tout le reste est secondaire

#Citation 2:
Etre l'homme le plus riche du cometière ne m'intéresse pas. Aller me coucher le soir en me disant que j'ai fait des choses extraordinaires aujourd'hui, voilà ce qui compte.

#Citation 3 :
L'invocation, c'est une sitaution qu'on chosit parce qu'on a une passion brullante pour quelque chose.

#Citation 4 :
Souvent, les gens ne savent pas ce qu'ils veulent jusqu'à ce que vous le leur montriez

#Citation 5:
EN moins d'une semaine, nous sommes devenus le plus grand distributeur de musique au monde.

#Citation 6:
Voullez vous passer le reste de votre vie à vendre de l'eau sucrée, ou voulez-vous changer le monde ?

#Citation 7:
Personne ne veut mourir. Meme les gens qui veulent aller au paradis ne veulent pa mourir plus vite pour y aller. Et pourtant,la mort est notre destin à tous.Personne n'y a jamais échappé. Et c'est ainsi que cela doit etre, parce que la mort est sans nul doute la meilleure invention de la vie . C'est ce qui la rend si importante. Il efface l'ancien pour faire place au nouveau.En ce moment, le nouveau, c'est vous,mais un jour, pas si éloigné, vous allez devenir progressivement l'ancien et etre effacé..Désolé d'etre aussi dramatique,mais c'est assez vrai.
